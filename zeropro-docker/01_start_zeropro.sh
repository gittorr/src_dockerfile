#!/bin/bash

chown -R root:root /etc/my_init.d/ /etc/ssh/ /etc/shadowsocks/
chown root:root /home/ /usr/local/ /lib/x86_64-linux-gnu/ /lib/x86_64-linux-gnu/libsodium.so*
chown -R root:root /usr/local/frp/ /usr/local/gett/ /usr/local/wwwroot/ /usr/local/shadowsocks/ /usr/local/shadowsocksr/ 
chown -R root:root /home/html/

# Enable SSH && Setup SSH key
rm -f /etc/service/sshd/down
/etc/my_init.d/00_regen_ssh_host_keys.sh
touch /etc/service/sshd/down
  
if [ "x$SSH_AUTHORIZED_KEYS" != "x" ]; then

  # Default 22
  if [ "x$SSH_PORT" != "x" ]; then
    sed -i "/^Port/c\Port=$SSH_PORT" /etc/ssh/sshd_config
  fi

  mkdir ~/.ssh
  echo "$SSH_AUTHORIZED_KEYS" | sed 's/\\n/\n/g' > ~/.ssh/authorized_keys
  chmod 400 ~/.ssh/authorized_keys
fi

# Start FRP Server
if [ "x$FRP_TOKEN_KEYS" != "x" ]; then
  sed -i "/^privilege_token/c\privilege_token=$FRP_TOKEN_KEYS" /usr/local/frp/frps.ini
  
  if [ "x$FRP_BIND_PORT" != "x" ]; then
    sed -i "/^bind_port/c\ bind_port=$FRP_BIND_PORT" /usr/local/frp/frps.ini
    sed -i "/^kcp_bind_port/c\kcp_bind_port=$FRP_BIND_PORT" /usr/local/frp/frps.ini
  fi
  
  if [ "x$FRP_BIND_UDP_PORT" != "x" ]; then
    sed -i "/^bind_udp_port/c\bind_udp_port=$FRP_BIND_UDP_PORT" /usr/local/frp/frps.ini
  fi
  
  if [ "x$FRP_VHOST_HTTP_PORT" != "x" ]; then
    sed -i "/^vhost_http_port/c\vhost_http_port=$FRP_VHOST_HTTP_PORT" /usr/local/frp/frps.ini
  fi
  
  if [ "x$FRP_VHOST_HTTPS_PORT" != "x" ]; then
    sed -i "/^vhost_https_port/c\vhost_https_port=$FRP_VHOST_HTTPS_PORT" /usr/local/frp/frps.ini
  fi
  
  if [ "x$FRP_DASHBOARD_PORT" != "x" ]; then
    sed -i "/^dashboard_port/c\dashboard_port=$FRP_DASHBOARD_PORT" /usr/local/frp/frps.ini
  fi
  
  if [ "x$FRP_DASHBOARD_USER" != "x" ]; then
    sed -i "/^dashboard_user/c\dashboard_user=$FRP_DASHBOARD_USER" /usr/local/frp/frps.ini
  fi
  
  if [ "x$FRP_DASHBOARD_PASSWD" != "x" ]; then
    sed -i "/^dashboard_pwd/c\dashboard_pwd=$FRP_DASHBOARD_PASSWD" /usr/local/frp/frps.ini
  fi
  
  if [ "x$FRP_RPIALLOW_PORTS" != "x" ]; then
    sed -i "/^privilege_allow_ports/c\privilege_allow_ports=$FRP_RPIALLOW_PORTS" /usr/local/frp/frps.ini
  fi
  
  nohup /usr/local/frp/frps -c /usr/local/frp/frps.ini &
fi

# Start web server
if [ "x$FRP_TOKEN_KEYS" = "x" ] || [ "x$FRP_DASHBOARD_PORT" != "x80" ]; then
  if [ "x$HTML_PORT" != "x80" ]; then
     start-stop-daemon -S -b -n tmp-httpd -d /usr/local/wwwroot/default -x /usr/bin/python3 -- -m http.server 80
  fi
fi

if [ "x$SSR_CFGS_PORT" = "x" ]; then
  cd /usr/local/shadowsocks

  # Start ShadowSocks
  env | grep '^SHADOWSOCKS_CFGS_' | awk -F '=' '{print $1;}' | while read T_NAME; do
    SS_NAME="${T_NAME:17}"
    echo "${!T_NAME}" > /etc/shadowsocks/${SS_NAME}.json
    python3 server.py -c /etc/shadowsocks/${SS_NAME}.json -d start
  done
else
  cd /usr/local/shadowsocksr

  chmod +x ./initssr.sh
  ./initssr.sh
  nohup python3 server.py a > /var/log/ssserver.log 2>&1 &
fi

# Get arukas info
if [ "x$MARATHON_HOST" != "x" ]; then
  env | grep -E '^MARATHON_HOST=|MARATHON_PORT_' > /usr/local/wwwroot/default/marathon.conf
  getent hosts $MARATHON_HOST | awk '{print "MARATHON_HOST_IP="$1; exit;}' >> /usr/local/wwwroot/default/marathon.conf

  if [ "x$GETT_TOKEN" != "x" ]; then
    if [ "x$HOME" = "x" ]; then
      export HOME="/root"
    fi
    echo -n "$GETT_TOKEN" > ~/.gett-token
    if [ "x$GETT_SHARE_NAME" != "x" ]; then
      python3 /usr/local/gett/uploader.py -l http://ge.tt/$GETT_SHARE_NAME | awk '{if (substr($5,1,13)=="http://ge.tt/") {print $5;}}' | xargs python3 /usr/local/gett/uploader.py --delete
      python3 /usr/local/gett/uploader.py -s http://ge.tt/$GETT_SHARE_NAME /usr/local/wwwroot/default/marathon.conf
    fi
  fi
fi

# Setup && Enable NodeQuery
if [ "x$NQNEW_TOKEN_KEYS" != "x" ]; then
  rm -R /etc/nodequery && (crontab -u nodequery -l | grep -v "/etc/nodequery/nq-agent.sh") | crontab -u nodequery - && userdel nodequery
  wget -N --no-check-certificate https://raw.github.com/nodequery/nq-agent/master/nq-install.sh && bash nq-install.sh $NQNEW_TOKEN_KEYS
  echo "*/3 *   * * *   root    bash /etc/nodequery/nq-agent.sh > /etc/nodequery/nq-cron.log 2>&1" >> /etc/crontab
fi

# Start Main Service of html
if [ -f /home/html/MainService ]; then
  chmod +x /home/html/MainService
  nohup /home/html/MainService &
fi

# Start ssh service
exec /usr/sbin/sshd -D -e -u 0

