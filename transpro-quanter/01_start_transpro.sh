#!/bin/bash

chown -R root:root /etc/my_init.d/ /etc/ssh/ 
chown root:root /home/ /usr/local/ 
chown -R root:root /home/Quanter/
chown -R root:root /home/FutuOpenD/

# Setup something
chmod +x /home/Quanter/setxmlpara
chmod +x /home/Quanter/futucmd

curr_pwd=`pwd`

# Make a user-config
cd /home/Config
Futunn_dir=$(echo /home/Config/$FUTU_login_nnid)
mkdir -p $Futunn_dir
cp *.xml $Futunn_dir

cd /home/FutuOpenD
if [ ! -f /home/FutuOpenD/FutuHistData.xml ]; then
  ln -s $Futunn_dir/FutuHistData.xml FutuHistData.xml
fi
cd $curr_pwd

# Set Paramters for FutuOpenD
setxmlpara ip 127.0.0.1 $Futunn_dir/FutuOpenD.xml
setxmlpara push_proto_type 0 $Futunn_dir/FutuOpenD.xml 
setxmlpara lang chs $Futunn_dir/FutuOpenD.xml
setxmlpara hist_data_dir /home/Histdata $Futunn_dir/FutuHistData.xml

if [ "x$FUTU_login_nnid" != "x" ]; then
  setxmlpara login_account $FUTU_login_nnid $Futunn_dir/FutuOpenD.xml
  setxmlpara nnid $FUTU_login_nnid $Futunn_dir/FutuHistData.xml
fi

if [ "x$FUTU_login_pwd_md5" != "x" ]; then
  setxmlpara login_pwd_md5 $FUTU_login_pwd_md5 $Futunn_dir/FutuOpenD.xml
fi

if [ "x$FUTU_api_port" != "x" ]; then
  setxmlpara api_port $FUTU_api_port $Futunn_dir/FutuOpenD.xml
else
  setxmlpara api_port 10109 $Futunn_dir/FutuOpenD.xml
fi

if [ "x$FUTU_telnet_port" != "x" ]; then
  setxmlpara telnet_port $FUTU_telnet_port $Futunn_dir/FutuOpenD.xml
  sed -i "/^port/c\port=$FUTU_telnet_port" /home/Quanter/futucmd
else
  setxmlpara telnet_port 20209 $Futunn_dir/FutuOpenD.xml
  sed -i "/^port/c\port=20209" /home/Quanter/futucmd
fi

if [ "x$FUTU_histdata" != "x" ]; then
  setxmlpara enable $FUTU_histdata $Futunn_dir/FutuHistData.xml
else
  setxmlpara enable 0 $Futunn_dir/FutuHistData.xml
fi

if [ "x$FUTU_hist_begtime" != "x" ]; then
  setxmlpara begin_time $FUTU_hist_begtime $Futunn_dir/FutuHistData.xml
else
  setxmlpara begin_time 0800 $Futunn_dir/FutuHistData.xml
fi

if [ "x$FUTU_hist_endtime" != "x" ]; then
  setxmlpara end_time $FUTU_hist_endtime $Futunn_dir/FutuHistData.xml
else
  setxmlpara end_time 2300 $Futunn_dir/FutuHistData.xml
fi

if [ "x$FUTU_hist_market" != "x" ]; then
  setxmlpara market $FUTU_hist_market $Futunn_dir/FutuHistData.xml
else
  setxmlpara market hk\|cn $Futunn_dir/FutuHistData.xml
fi

if [ "x$FUTU_hist_ktype" != "x" ]; then
  setxmlpara kline_type $FUTU_hist_ktype $Futunn_dir/FutuHistData.xml
else
  setxmlpara kline_type day $Futunn_dir/FutuHistData.xml
fi

# Start FutuOpenD & FutuHistData
nohup FutuOpenD -console=0 -cfg_file=$Futunn_dir/FutuOpenD.xml -hist_data_cfg_file=$Futunn_dir/FutuHistData.xml &
nohup FutuHistData >>/home/Histdata/FutuHistData.log 2>&1 &

# Start Main Service of Quanter
if [ -f /home/Quanter/MainService ]; then
  chmod +x /home/Quanter/MainService
  nohup /home/Quanter/MainService &
fi

# Enable SSH && Setup SSH key
rm -f /etc/service/sshd/down
/etc/my_init.d/00_regen_ssh_host_keys.sh
touch /etc/service/sshd/down
  
if [ "x$SSH_AUTHORIZED_KEYS" != "x" ]; then

  # Default 22
  if [ "x$SSH_PORT" != "x" ]; then
    sed -i "/^Port/c\Port=$SSH_PORT" /etc/ssh/sshd_config
  fi

  mkdir ~/.ssh
  echo "$SSH_AUTHORIZED_KEYS" | sed 's/\\n/\n/g' > ~/.ssh/authorized_keys
  chmod 400 ~/.ssh/authorized_keys
fi

# Start ssh service
exec /usr/sbin/sshd -D -e -u 0

